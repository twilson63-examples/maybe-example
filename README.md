Maybe is a way to keep your functions safe, from unexpected input. By leveraging a predicate function to test your input before you call the function. The result is type safety in a language that supports multiple types.

First Principles

* When exposing functions with arguments to external users, you can not trust users to do the right thing.
* Manually checking types, interfaces and boundries is tedious and creates unreadable code.
* Bringing in a whole new language to solve this issue is a large undertaking.
* Using functional techniques like the Maybe Monad could go a long way.

The Maybe Monad allows you to pass a predicate function that will validate your functions input, then return a Just or Nothing, in which you can unwrap using the `option` method to get your value.

``` js
const safeFoo = Maybe(pred).map(foo)

const result = safeFoo('beep').option('nope')
console.log(result) // boop
```

The `predicate` function can check for type, or type and shape, or type shape and bounds. For example, maybe you want a string, but it could be a specific format, like all uppercase, and maybe a specific link like only 50 characters. Your predicate function, gives you the ability to check all requirements. Using a library like `yup` can give you the ability to do more than just type and shape checking, it can give you the ability to add bounds checking as well.

> What if I have more than one argument? 

The `crocks` library has functions that make validing multiple argument functions simple with safeLift functions.

`safeLift` takes a predicate and the function as one set of arguments.

and `liftA2` and `liftA3` will take two and three predicates, then the function.

``` js
const safeFoo = safeLift(pred, foo)
console.log(safeFoo('beep').option('nope')) // 'boop'
```

The great thing about predicate functions, is that they get passed the value and can inspect the value and only have to return a true or false. If the predicate returns true then the function is invoked with the value if the result is false, the function is not invoked an a Nothing is returned. This creates a safe function. 

For example, you may want to ensure that a Person object is passed to your function, and that person object will have specific properties, using the predicate you can make sure that it only has those properties, otherwise return false. Maybe you only want the user to provide an integer between 1 and 10. You can use the predicate function to make sure it is an interger as well as between the range of 1 and 10. 

Maybe's gives you the ability to check type, shape and bounds to make sure your input is exactly what you want it to be.

Maybe not only let you write maintainable code and protect it, it can also empower you to protect libraries and functions that you do not have control over. 
