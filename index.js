const { isNumber, safeLift } = require('crocks')
const fetch = require('node-fetch')

function promise1(id) {
  return fetch(`https://jsonplaceholder.typicode.com/todos/${id}`)
    .then(res => res.json())
}

const getTodo = safeLift(isNumber, promise1)
Promise.all([
  getTodo(1),
  getTodo(2),
  getTodo('3')
]).then(results => 
  results
    .map(m => m.option(Promise.resolve({})))
    .map(p => p.then(console.log))
  )
